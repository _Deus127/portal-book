package level

import (
	"bitbucket.org/_Deus127/portal-book/ch7-vline/engine/entities"
	"bitbucket.org/_Deus127/portal-book/ch7-vline/engine/geom"
)

// Level Defaults
const (
	DefaultPath        = "content/levels/"
	DefaultLevelWidth  = 500
	DefaultLevelHeight = 500
)

// Level is a world map / level.
type Level struct {
	Vertices []*geom.Vertex
	Lines    []*geom.Line
	Sectors  []*geom.Sector

	Entities []*entities.Entity

	Name          string
	Filepath      string
	X, Y          float64
	Width, Height float64
}

// NewLevel returns a new level instance.
func NewLevel(name string, filepath string, width float64, height float64) *Level {
	m := &Level{
		Name:     name,
		Filepath: filepath,
		Width:    width,
		Height:   height,
	}
	m.X = -m.Width / 2.0
	m.Y = -m.Height / 2.0
	return m
}

// NumWallsAndPortals returns the wall (non-portal) and portal line count for this level.
func (l *Level) NumWallsAndPortals() (cntWalls int, cntPortals int) {
	for _, line := range l.Lines {
		if line.IsPortal() {
			cntPortals++
		} else {
			cntWalls++
		}
	}
	return cntWalls, cntPortals
}

// LinesForSectorID returns all lines in the level touching the requested sector.
func (l *Level) LinesForSectorID(sectorID int) []*geom.Line {
	out := []*geom.Line{}
	for _, line := range l.Lines {
		if (line.Side0.TouchesSectorID == sectorID) ||
			(line.Side1 != nil && line.Side1.TouchesSectorID == sectorID) {

			out = append(out, line)
		}
	}
	return out
}
