package game

import (
	"fmt"
	"math"

	"bitbucket.org/_Deus127/portal-book/ch7-vline/engine/camera"
	"bitbucket.org/_Deus127/portal-book/ch7-vline/engine/drawer"
	"bitbucket.org/_Deus127/portal-book/ch7-vline/engine/entities"
	"bitbucket.org/_Deus127/portal-book/ch7-vline/engine/level"
	"bitbucket.org/_Deus127/portal-book/ch7-vline/engine/mathutils"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// Game gives access to the game.
type Game struct {
	DebugCamera *camera.Camera
	debugStage  camera.DebugStage
	Level       *level.Level

	Player        *entities.Entity
	MoveSpeed     float64
	TurnRate      float64
	MaxTurnSpeed  float64
	MouseDeadZone float64

	centerPos  pixel.Vec
	mousePos   pixel.Vec
	mouseDelta pixel.Vec
	moveVec    *pixel.Vec
}

// NewGame returns a new game instance.
func NewGame(win *pixelgl.Window) *Game {
	// fmt.Print("Loading Game...")
	g := Game{
		Level:         level.LoadFromFile(level.DefaultPath + "TestMap.map"),
		MoveSpeed:     2.0,
		TurnRate:      3.0,
		MaxTurnSpeed:  4.0,
		MouseDeadZone: 0.05,
		moveVec:       &pixel.Vec{},
		debugStage:    camera.PERSPECTIVE,
	}
	g.Player = g.Level.Entities[0]
	g.DebugCamera = camera.NewCamera(win.Bounds().W(), win.Bounds().H(), 90.0*mathutils.DegToRad)

	g.centerPos = pixel.V(g.DebugCamera.RenderWidth/2, g.DebugCamera.RenderHeight/2)
	win.SetCursorVisible(false)
	win.SetMousePosition(g.centerPos)
	g.mousePos = g.centerPos
	g.mouseDelta = pixel.V(0, 0)
	// fmt.Println("OK")
	return &g
}

// HandleInput handles input to the game.
func (g *Game) HandleInput(win *pixelgl.Window, dt float64) {
	if win.JustPressed(pixelgl.Key1) {
		g.debugStage = camera.OVERHEAD
	} else if win.JustPressed(pixelgl.Key2) {
		g.debugStage = camera.TRANSLATED
	} else if win.JustPressed(pixelgl.Key3) {
		g.debugStage = camera.ROTATED
	} else if win.JustPressed(pixelgl.Key4) {
		g.debugStage = camera.PERSPECTIVE
	}

	// NO MOVEMENT UNLESS PLAYER PERSPECTIVE
	if g.debugStage != camera.PERSPECTIVE {
		return
	}

	// vFOV (Zoom / Iron Sight)
	if win.Pressed(pixelgl.MouseButtonRight) {
		g.DebugCamera.SetFOV(60 * mathutils.DegToRad)
	} else if win.JustReleased(pixelgl.MouseButtonRight) {
		g.DebugCamera.SetFOV(90 * mathutils.DegToRad)
	}

	// move
	g.moveVec = &pixel.Vec{}

	if win.Pressed(pixelgl.KeyW) {
		g.moveVec.Y = g.MoveSpeed
	}
	if win.Pressed(pixelgl.KeyA) {
		g.moveVec.X = -g.MoveSpeed
	}
	if win.Pressed(pixelgl.KeyS) {
		g.moveVec.Y = -g.MoveSpeed
	}
	if win.Pressed(pixelgl.KeyD) {
		g.moveVec.X = g.MoveSpeed
	}

	// turning
	g.mousePos = win.MousePosition()
	g.mouseDelta = g.mousePos.Sub(win.MousePreviousPosition())
}

// Update updates the game (should be called once per frame).
func (g *Game) Update(win *pixelgl.Window, draw *drawer.Drawer, dt float64) {
	// rotate move vector and apply
	anglesin := math.Sin(-g.Player.Angle)
	anglecos := math.Cos(-g.Player.Angle)
	mvRot := pixel.V(
		(g.moveVec.X*anglecos)-(g.moveVec.Y*anglesin),
		(g.moveVec.X*anglesin)+(g.moveVec.Y*anglecos))
	g.Player.Move(mvRot.X*dt, mvRot.Y*dt, g.Level.Sectors)

	// turn relative to player
	if g.mousePos.X < 0 || g.mousePos.X > g.DebugCamera.RenderWidth-1 ||
		g.mousePos.Y < 0 || g.mousePos.Y > g.DebugCamera.RenderHeight-1 {

		win.SetMousePosition(g.centerPos.Add(g.mouseDelta)) // set to be picked up as "previous" position on next cycle
		win.SetMousePosition(g.centerPos)                   // set to be picked up as "current" position on next cycle
	}

	if math.Abs(g.mouseDelta.X) > g.MouseDeadZone {
		turn := pixel.Clamp(g.TurnRate*g.mouseDelta.X, -g.MaxTurnSpeed, g.MaxTurnSpeed)
		g.Player.Angle += turn * dt
	}
}

// Draw lets the game draw.
func (g *Game) Draw(win *pixelgl.Window, draw *drawer.Drawer) {
	// win.Clear(colornames.Black)
	g.DebugCamera.Draw(win, draw,
		g.Player.Position.X(), g.Player.Position.Y(), 1, // Z
		g.Player.Angle, g.Player.SectorID, g.Level.Sectors, g.debugStage)
	fmt.Fprintf(draw.TxtDrawer, "\n\n1 - OVERHEAD\n2 - TRANSLATED\n3 - ROTATED\n4 - PERSPECTIVE")
	draw.TxtDrawer.Draw(win, pixel.IM.Scaled(draw.TxtDrawer.Orig, draw.TxtDrawerScale))
	draw.TxtDrawer.Clear()
}

// OnShutdown lets the game know the engine is shutting down.
func (g *Game) OnShutdown() {
}
