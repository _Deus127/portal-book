package geom

import (
	"github.com/faiface/pixel"
)

// Vertex holds a vertex definition.
type Vertex struct {
	*pixel.Vec
}

// NewVertex returns a new vertex instance.
func NewVertex(x float64, y float64) *Vertex {
	return &Vertex{
		Vec: &pixel.Vec{
			X: x,
			Y: y,
		},
	}
}
