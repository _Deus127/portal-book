package engine

// sensible defaults that may be updated by caller.
var (
	CFGWidth       = 1280
	CFGHeight      = 720
	CFGScale       = 1.0
	CFGFullscreen  = false
	CFGVSync       = false
	CFGUndecorated = false
	CFGShowFPS     = true
)
