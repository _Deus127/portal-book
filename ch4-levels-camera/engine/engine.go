package engine

import (
	"fmt"
	"time"

	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/colorize"
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/drawer"
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/framerate"
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/game"
	"golang.org/x/image/colornames"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// VERSION is the engine version.
const VERSION = "DEV"

// Gruel holds an engine instance.
type Gruel struct {
	Title string

	// video settings
	Width       int
	Height      int
	Scale       float64
	Fullscreen  bool
	VSync       bool
	Undecorated bool
	ShowFPS     bool

	// windows and drawer
	Win    *pixelgl.Window
	Drawer *drawer.Drawer

	// FPS tracking
	LastFrameTime    time.Time
	DeltaTime        float64
	FramerateMonitor *framerate.Monitor

	Game *game.Game
}

// NewGruel returns a new engine instance.
func NewGruel(title string) *Gruel {
	colorize.InitForTerminal()

	fmt.Printf("%s%sGruel Engine v%s\n© 2020 Dan Allessi %s%s\n",
		colorize.BlueBG, colorize.White, VERSION, colorize.BlackBG, colorize.Green)

	return &Gruel{
		Title:      title,
		Width:      CFGWidth,
		Height:     CFGHeight,
		Scale:      CFGScale,
		Fullscreen: CFGFullscreen,
		VSync:      CFGVSync,
		ShowFPS:    CFGShowFPS,
	}
}

func (g *Gruel) getTitle() string {
	return g.Title + " v" + VERSION
}

// Start kicks off the engine process.
func (g *Gruel) Start() {
	pixelgl.Run(g.run)
}

func (g *Gruel) shouldStop() bool {
	return g.Win.Closed() || g.Win.JustPressed(pixelgl.KeyEscape)
}

func (g *Gruel) run() {
	// setup
	g.initWindow()
	g.Drawer = drawer.NewDrawer(g.Win)
	g.FramerateMonitor = framerate.NewMonitor()
	g.loadContent()
	// main loop
	for !g.shouldStop() {
		// delta time and fps
		g.DeltaTime = time.Since(g.LastFrameTime).Seconds()
		g.LastFrameTime = time.Now()
		// render a frame
		g.Win.Clear(colornames.Antiquewhite)
		g.handleInput()
		g.update()
		g.draw()
		g.Win.Update() // flip the frame buffer
	}
	g.shutdown()
}

func (g *Gruel) handleInput() {
	g.Game.HandleInput(g.Win, g.DeltaTime)
}

func (g *Gruel) update() {
	if g.ShowFPS {
		g.FramerateMonitor.Update(g.DeltaTime)
	}
	g.Game.Update(g.Win, g.Drawer, g.DeltaTime)
}

func (g *Gruel) draw() {
	g.Game.Draw(g.Win, g.Drawer)
	if g.ShowFPS {
		g.FramerateMonitor.Draw(g.Win, g.Drawer)
	}
}

func (g *Gruel) initWindow() {
	fmt.Print("Init Window...")
	cfg := pixelgl.WindowConfig{
		Title: g.getTitle(),
		Bounds: pixel.R(0, 0,
			float64(g.Width)*g.Scale,
			float64(g.Height)*g.Scale),
		VSync:       g.VSync,
		Undecorated: g.Undecorated,
	}

	if g.Fullscreen {
		cfg.Monitor = pixelgl.PrimaryMonitor()
	}

	var err error
	g.Win, err = pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}
	g.Win.SetSmooth(true)
	fmt.Println("OK")
}

func (g *Gruel) loadContent() {
	fmt.Print("Load Content...")
	g.Game = game.NewGame(g.Win)
	fmt.Println("OK")
}

func (g *Gruel) shutdown() {
	fmt.Printf("%sShutting down...", colorize.Red)
	g.Game.OnShutdown()
	fmt.Printf("OK%s\n", colorize.Reset)
}
