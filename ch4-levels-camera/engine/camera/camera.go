package camera

import (
	"image"

	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/drawer"
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/geom"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// Camera represents a rendering viewpoint in the world.
type Camera struct {
	RenderWidth  float64
	RenderHeight float64

	renderBuffer *image.RGBA
	RenderTarget *pixel.Sprite

	portalQueue        []*Portal
	maxSectorRenderCnt int

	debugX     float64
	debugY     float64
	debugScale float64
}

// NewCamera returns a new camera instance.
func NewCamera(renderWidth float64, renderHeight float64) *Camera {

	buffer := image.NewRGBA(image.Rect(0, 0, int(renderWidth), int(renderHeight)))
	p := pixel.PictureDataFromImage(buffer)
	renderTarget := pixel.NewSprite(p, p.Bounds())

	c := Camera{
		RenderWidth:        renderWidth,
		RenderHeight:       renderHeight,
		renderBuffer:       buffer,
		RenderTarget:       renderTarget,
		maxSectorRenderCnt: 1,
		debugX:             renderWidth / 2,
		debugY:             renderHeight / 2,
		debugScale:         20.0,
	}
	return &c
}

// Draw draws/renders a frame from the provided position (fed programmatically or via an Entity).
func (c *Camera) Draw(win *pixelgl.Window, draw *drawer.Drawer,
	x float64, y float64, z float64,
	angle float64,
	sectorID int, sectorList []*geom.Sector) {

	draw.IMDrawer.Clear()
	draw.IMDrawer.SetMatrix(pixel.IM)
	c.renderFrame(win, draw, x, y, z, angle, sectorID, sectorList)

	// update render target sprite w/ buffer
	p := pixel.PictureDataFromImage(c.renderBuffer)
	c.RenderTarget.Set(p, p.Bounds())
	c.RenderTarget.Draw(win, pixel.IM.Moved(pixel.V(c.RenderWidth/2, c.RenderHeight/2)))

	draw.IMDrawer.Draw(win)
}
