package camera

import (
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/drawer"
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/geom"
	"golang.org/x/image/colornames"

	"github.com/faiface/pixel/pixelgl"
)

func (c *Camera) _initRenderFrame() (renderWidth int, renderHeight int,
	renderedSectorCnts map[int]int) {

	renderWidth = int(c.RenderWidth)
	renderHeight = int(c.RenderHeight)
	renderedSectorCnts = map[int]int{}
	return renderWidth, renderHeight, renderedSectorCnts
}

func (c *Camera) renderFrame(win *pixelgl.Window, draw *drawer.Drawer,
	x float64, y float64, z float64,
	angle float64,
	sectorID int, sectorList []*geom.Sector) {

	renderWidth, renderHeight, renderedSectorCnts := c._initRenderFrame()

	// begin rendering from the prescribed position (push our sector into the queue with the full width available)
	c.pushPortalQueue(&Portal{
		SectorID: sectorID,
		LeftX:    0,
		RightX:   renderWidth - 1,
	})

	// process all portals in the order they are added
	for len(c.portalQueue) > 0 {
		currPortal := c.popPortalQueue()
		currSector := sectorList[currPortal.SectorID]

		// abort if we've already drawn this sector too many times (i.e. mirrors)
		if renderCnt, ok := renderedSectorCnts[currSector.ID]; ok && renderCnt > c.maxSectorRenderCnt {
			continue
		}
		// render the current sector as seen through the current portal
		c._renderSectorFromThrough(win, draw, x, y, z, angle,
			currPortal, currSector, sectorList, renderWidth, renderHeight)
		renderedSectorCnts[currSector.ID]++
	}
}

func (c *Camera) _renderSectorFromThrough(win *pixelgl.Window, draw *drawer.Drawer,
	x float64, y float64, z float64,
	angle float64,
	portal *Portal, sector *geom.Sector, sectorList []*geom.Sector,
	renderWidth int, renderHeight int) {

	// DEBUG - CAM POS
	c._debugPointScaled(draw, 0, 0, colornames.Red)

	for _, line := range sector.Lines {

		// DEBUG - SECTOR OVERHEAD
		c._debugLineScaled(draw, line.V0.Vec.X, line.V0.Vec.Y, line.V1.Vec.X, line.V1.Vec.Y, colornames.Olive)

		// do we have a neighbor through this wall?
		neighbor, _, _ := c._getNeighborInfo(x, y, z, line, portal, sectorList)

		// schedule the neighboring sector for rendering within the portal window formed by this wall
		if neighbor != nil {
			c.pushPortalQueue(&Portal{
				SectorID: neighbor.ID,
				LeftX:    0,
				RightX:   renderWidth - 1,
			})
		}
	}
}
