package level

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/entities"
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/geom"

	"github.com/go-gl/mathgl/mgl64"
)

// LoadFromFile deserializes the level from file.
func LoadFromFile(path string) *Level {
	// open file
	readFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	// read lines from file
	var fileLines []string
	for fileScanner.Scan() {
		fileLines = append(fileLines, fileScanner.Text())
	}
	readFile.Close()
	// parse file
	splitPath := strings.Split(path, string(os.PathSeparator))
	mapName := splitPath[len(splitPath)-1]
	pathOnly := strings.ReplaceAll(path, mapName, "")
	lev := NewLevel(mapName, pathOnly, DefaultLevelWidth, DefaultLevelHeight)

	for _, line := range fileLines {

		if len(line) <= 1 { // ignore empty lines
			continue
		}

		switch strings.ToUpper(line[:1]) {
		case "#": // comment
			continue

		case "M": // metadata
			splitLine := strings.Split(line, "|")
			for chidx, chunk := range splitLine[1:] {

				// fmt.Printf("META I: %d V: '%s'\n", chidx, chunk)

				switch chidx {
				case 0:
					parseMetadataChunk(chunk, lev)
					break
				default:
					panic("Unknown chunk")
				}
			}
			break

		case "V": // vertex
			splitLine := strings.Split(line, "|")
			for chidx, chunk := range splitLine[1:] {

				// fmt.Printf("VERTEX I: %d V: '%s'\n", chidx, chunk)

				switch chidx {
				case 0:
					parseVertexXYChunk(chunk, lev)
					break
				default:
					panic("Unknown chunk")
				}
			}
			break

		case "L": // line
			splitLine := strings.Split(line, "|")
			var v0, v1 *geom.Vertex
			var side0 *geom.Side
			var side1 *geom.Side
			for chidx, chunk := range splitLine[1:] {

				// fmt.Printf("LINE I: %d V: '%s'\n", chidx, chunk)

				switch chidx {
				case 0:
					v0, v1 = parseLineVerticesChunk(chunk, lev)
					break
				case 1:
					side0, side1 = parseLineSidesChunk(chunk, lev)
					break
				default:
					panic("Unknown chunk")
				}
			}
			// fmt.Printf("\nAdding LINE #%d: %#v %#v %#v %#v\n",
			// 	len(lev.Lines), v0, v1, side0, side1)
			lev.Lines = append(lev.Lines, geom.NewLine(
				v0.Vec.X, v0.Vec.Y, v1.Vec.X, v1.Vec.Y, side0, side1))
			break

		case "S": // sector
			splitLine := strings.Split(line, "|")
			var floorHeight, ceilingHeight float64
			for chidx, chunk := range splitLine[1:] {

				// fmt.Printf("SECTOR I: %d V: '%s'\n", chidx, chunk)

				switch chidx {
				case 0:
					floorHeight, ceilingHeight = parseSectorHeightChunk(chunk, lev)
					break
				default:
					panic("Unknown chunk")
				}
			}
			// create the sector
			lev.Sectors = append(lev.Sectors, geom.NewSector(
				len(lev.Sectors),
				floorHeight, ceilingHeight,
				lev.LinesForSectorID(len(lev.Sectors))))
			break

		case "E": // entity
			splitLine := strings.Split(line, "|")
			var eType string
			var eX, eY, eAngle float64
			var eSectorID int
			for chidx, chunk := range splitLine[1:] {

				// fmt.Printf("ENTITY I: %d V: '%s'\n", chidx, chunk)

				switch chidx {
				case 0:
					eType = chunk
					break
				case 1:
					eX, eY, eAngle = parseEntityTransformChunk(chunk)
					break
				case 2:
					pEntry, err := strconv.ParseInt(chunk, 10, 64)
					if err != nil {
						panic(err)
					}
					eSectorID = int(pEntry)
				default:
					panic("Unknown chunk")
				}
			}
			lev.Entities = append(lev.Entities, entities.NewEntity(
				eType,
				mgl64.Vec3{eX, eY, (lev.Sectors[eSectorID].FloorHeight)},
				mgl64.Vec3{0, 0, 0},
				eAngle, eSectorID))
			break
		}
	}
	return lev
}

func parseMetadataChunk(chunk string, lev *Level) {
	splitChunk := strings.Split(chunk, " ")
	for idx, entry := range splitChunk {

		pEntry, err := strconv.ParseFloat(entry, 64)
		if err != nil {
			panic(err)
		}

		if idx == 0 {
			lev.Width = pEntry
		} else if idx == 1 {
			lev.Height = pEntry
		}
	}
}

func parseVertexXYChunk(chunk string, lev *Level) {
	splitChunk := strings.Split(chunk, " ")
	newV := geom.NewVertex(0, 0)
	for idx, entry := range splitChunk {

		pEntry, err := strconv.ParseFloat(entry, 64)
		if err != nil {
			panic(err)
		}

		if idx == 0 {
			newV.Vec.X = pEntry
		} else if idx == 1 {
			newV.Vec.Y = pEntry
		}
	}
	lev.Vertices = append(lev.Vertices, newV)
}

func parseLineVerticesChunk(chunk string, lev *Level) (v0 *geom.Vertex, v1 *geom.Vertex) {
	splitChunk := strings.Split(chunk, " ")
	for idx, entry := range splitChunk {

		pEntry, err := strconv.ParseInt(entry, 10, 64)
		if err != nil {
			panic(err)
		}

		if idx == 0 {
			v0 = lev.Vertices[int(pEntry)]
		} else if idx == 1 {
			v1 = lev.Vertices[int(pEntry)]
		}
	}
	return
}

func parseLineSidesChunk(chunk string, lev *Level) (s0 *geom.Side, s1 *geom.Side) {
	splitChunk := strings.Split(chunk, " ")
	var sectorID0, sectorID1 int
	for idx, entry := range splitChunk {

		pEntry, err := strconv.ParseFloat(entry, 64)
		if err != nil {
			panic(err)
		}

		switch idx {
		case 0:
			sectorID0 = int(pEntry)
			break
		case 1:
			sectorID1 = int(pEntry)
			break
		}
	}

	s0 = geom.NewSide(sectorID0)
	if sectorID1 >= 0 {
		s1 = geom.NewSide(sectorID1)
	}
	return s0, s1
}

func parseSectorHeightChunk(chunk string, lev *Level) (floorHeight float64, ceilingHeight float64) {
	splitChunk := strings.Split(chunk, " ")
	for idx, entry := range splitChunk {

		pEntry, err := strconv.ParseFloat(entry, 64)
		if err != nil {
			panic(err)
		}

		if idx == 0 {
			floorHeight = pEntry
		} else if idx == 1 {
			ceilingHeight = pEntry
		}
	}
	return floorHeight, ceilingHeight
}

func parseEntityTransformChunk(chunk string) (x float64, y float64, angle float64) {
	splitChunk := strings.Split(chunk, " ")
	for idx, entry := range splitChunk {

		pEntry, err := strconv.ParseFloat(entry, 64)
		if err != nil {
			panic(err)
		}

		if idx == 0 {
			x = pEntry
		} else if idx == 1 {
			y = pEntry
		} else if idx == 2 {
			angle = pEntry
		}
	}
	return x, y, angle
}
