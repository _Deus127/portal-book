package game

import (
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/camera"
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/drawer"
	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/level"

	"github.com/faiface/pixel/pixelgl"
)

// Game gives access to the game.
type Game struct {
	DebugCamera *camera.Camera
	Level       *level.Level
}

// NewGame returns a new game instance.
func NewGame(win *pixelgl.Window) *Game {
	// fmt.Print("Loading Game...")
	g := Game{
		Level: level.LoadFromFile(level.DefaultPath + "TestMap.map"),
	}
	g.DebugCamera = camera.NewCamera(win.Bounds().W(), win.Bounds().H())
	// fmt.Println("OK")
	return &g
}

// HandleInput handles input to the game.
func (g *Game) HandleInput(win *pixelgl.Window, dt float64) {
}

// Update updates the game (should be called once per frame).
func (g *Game) Update(win *pixelgl.Window, draw *drawer.Drawer, dt float64) {
}

// Draw lets the game draw.
func (g *Game) Draw(win *pixelgl.Window, draw *drawer.Drawer) {
	// win.Clear(colornames.Black)
	player := g.Level.Entities[0]
	g.DebugCamera.Draw(win, draw,
		player.Position.X(), player.Position.Y(), 1, // Z
		player.Angle, player.SectorID, g.Level.Sectors)
}

// OnShutdown lets the game know the engine is shutting down.
func (g *Game) OnShutdown() {
}
