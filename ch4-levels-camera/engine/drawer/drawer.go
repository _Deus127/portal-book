package drawer

import (
	"fmt"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"github.com/faiface/pixel/text"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font/basicfont"
)

// Drawer gives access to the 2D drawing elements.
type Drawer struct {
	TxtDrawer      *text.Text
	TxtDrawerScale float64
	IMDrawer       *imdraw.IMDraw
}

// NewDrawer returns a new drawer instance.
func NewDrawer(win *pixelgl.Window) *Drawer {
	d := &Drawer{}
	fmt.Print("Init IMDrawer...")
	d.IMDrawer = imdraw.New(nil)
	fmt.Println("OK")

	fmt.Print("Init TxtDrawer...")
	basicAtlas := text.NewAtlas(basicfont.Face7x13, text.ASCII)
	d.TxtDrawer = text.New(pixel.V(15, win.Bounds().H()-15), basicAtlas)
	d.TxtDrawer.Color = colornames.Darkgreen
	d.TxtDrawerScale = 1.5
	fmt.Println("OK")

	return d
}
