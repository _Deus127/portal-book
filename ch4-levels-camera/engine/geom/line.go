package geom

import "bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine/mathutils"

// Line holds a line definition.
type Line struct {
	V0, V1 *Vertex
	Side0  *Side
	Side1  *Side
}

// NewLine returns a new Line instance.
func NewLine(x0 float64, y0 float64, x1 float64, y1 float64, side0 *Side, side1 *Side) *Line {
	return &Line{
		V0:    NewVertex(x0, y0),
		V1:    NewVertex(x1, y1),
		Side0: side0,
		Side1: side1,
	}
}

// IsPortal lets the caller know if this line is a portal (has a neighboring sector behind it).
func (l *Line) IsPortal() bool {
	return (l.Side1 != nil)
}

// NeighborOppositeOf finds the sector ID on the opposite side of the portal relative to the provided position.
func (l *Line) NeighborOppositeOf(x float64, y float64) (neighborSectorID int) {
	if mathutils.PointSide( // find which side of the portal we're on and grab the opposite side's data
		x, y,
		l.V0.Vec.X, l.V0.Vec.Y,
		l.V1.Vec.X, l.V1.Vec.Y) < 0 {

		neighborSectorID = l.Side1.TouchesSectorID
	} else {
		neighborSectorID = l.Side0.TouchesSectorID
	}
	return neighborSectorID
}
