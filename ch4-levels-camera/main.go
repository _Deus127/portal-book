package main

import (
	"flag"
	"fmt"
	"runtime"

	"bitbucket.org/_Deus127/portal-book/ch4-levels-camera/engine"
)

func main() {
	flag.BoolVar(&engine.CFGFullscreen, "f", engine.CFGFullscreen, "fullscreen")
	flag.BoolVar(&engine.CFGVSync, "v", engine.CFGVSync, "vsync")
	flag.BoolVar(&engine.CFGUndecorated, "u", engine.CFGUndecorated, "undecorated window")
	flag.BoolVar(&engine.CFGShowFPS, "fps", engine.CFGShowFPS, "show fps")
	flag.IntVar(&engine.CFGWidth, "w", engine.CFGWidth, "width")
	flag.IntVar(&engine.CFGHeight, "h", engine.CFGHeight, "height")
	flag.Float64Var(&engine.CFGScale, "s", engine.CFGScale, "scale")
	flag.Parse()

	numCPU := runtime.NumCPU()
	// only way to see maxprocs is to set it and see the return value, then set it back
	maxProcs := runtime.GOMAXPROCS(numCPU)
	runtime.GOMAXPROCS(maxProcs)
	fmt.Printf("CPUs: %d MAXPROCS: %d\n", numCPU, maxProcs)

	gruel := engine.NewGruel("Apsalus Gruel Ch4")
	gruel.Start()
}
