package geom

// Sector holds a sector definition.
type Sector struct {
	ID            int
	FloorHeight   float64
	CeilingHeight float64
	Lines         []*Line
}

// NewSector returns a new sector instance.
func NewSector(
	id int,
	floorHeight float64,
	ceilingHeight float64,
	lines []*Line) *Sector {
	return &Sector{
		ID:            id,
		FloorHeight:   floorHeight,
		CeilingHeight: ceilingHeight,
		Lines:         lines,
	}
}
