package camera

import (
	"image"
	"math"

	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/drawer"
	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/geom"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// Camera represents a rendering viewpoint in the world.
type Camera struct {
	RenderWidth  float64
	RenderHeight float64

	renderBuffer *image.RGBA
	RenderTarget *pixel.Sprite

	hFOV float64 // fov (scaling) factors for perspective projection
	vFOV float64

	portalQueue        []*Portal
	maxSectorRenderCnt int

	debugX     float64
	debugY     float64
	debugScale float64

	nearZ    float64 // view frustum (clipping)
	farZ     float64
	nearSide float64
	farSide  float64
}

// NewCamera returns a new camera instance.
func NewCamera(renderWidth float64, renderHeight float64, fov float64) *Camera {

	buffer := image.NewRGBA(image.Rect(0, 0, int(renderWidth), int(renderHeight)))
	p := pixel.PictureDataFromImage(buffer)
	renderTarget := pixel.NewSprite(p, p.Bounds())

	c := Camera{
		RenderWidth:        renderWidth,
		RenderHeight:       renderHeight,
		renderBuffer:       buffer,
		RenderTarget:       renderTarget,
		maxSectorRenderCnt: 1,
		debugX:             renderWidth / 2,
		debugY:             renderHeight / 2,
		debugScale:         20.0,
		nearZ:              0.0001,
		farZ:               50.0,
		nearSide:           1.0,
		farSide:            3.0,
	}
	c.SetFOV(fov)
	return &c
}

// SetFOV sets the horizontal and vertical fields of view.
func (c *Camera) SetFOV(fov float64) {
	c.SetHorizFOV(fov)
	c.SetVertFOV(fov)
}

// SetHorizFOV sets the horizontal field of view.
func (c *Camera) SetHorizFOV(fov float64) {
	c.hFOV = (c.RenderWidth / 2) / math.Tan(fov/2.0)
}

// SetVertFOV sets the vertical field of view.
func (c *Camera) SetVertFOV(fov float64) {
	c.vFOV = (c.RenderHeight / 2) / math.Tan(fov/2.0)
}

// Draw draws/renders a frame from the provided position (fed programmatically or via an Entity).
func (c *Camera) Draw(win *pixelgl.Window, draw *drawer.Drawer,
	x float64, y float64, z float64,
	angle float64,
	sectorID int, sectorList []*geom.Sector, debugStage DebugStage) {

	draw.IMDrawer.Clear()
	draw.IMDrawer.SetMatrix(pixel.IM)
	c.renderFrame(win, draw, x, y, z, angle, sectorID, sectorList, debugStage)

	// update render target sprite w/ buffer
	p := pixel.PictureDataFromImage(c.renderBuffer)
	c.RenderTarget.Set(p, p.Bounds())
	c.RenderTarget.Draw(win, pixel.IM.Moved(pixel.V(c.RenderWidth/2, c.RenderHeight/2)))

	draw.IMDrawer.Draw(win)
}
