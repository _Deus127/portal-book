package camera

import (
	"image/color"

	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/drawer"
	"github.com/faiface/pixel"
)

func (c *Camera) _debugPointScaled(draw *drawer.Drawer, x float64, y float64, dColor color.Color) {
	draw.IMDrawer.Color = dColor
	draw.IMDrawer.Push(pixel.V(x*c.debugScale+c.debugX, y*c.debugScale+c.debugY))
	draw.IMDrawer.Circle(2.0, 0.0)
}

func (c *Camera) _debugLineScaled(draw *drawer.Drawer,
	x0 float64, y0 float64,
	x1 float64, y1 float64,
	dColor color.Color) {

	draw.IMDrawer.Color = dColor
	draw.IMDrawer.Push(
		pixel.V(x0*c.debugScale+c.debugX, y0*c.debugScale+c.debugY),
		pixel.V(x1*c.debugScale+c.debugX, y1*c.debugScale+c.debugY))
	draw.IMDrawer.Line(1.0)
}

func (c *Camera) _debugLine(draw *drawer.Drawer,
	x0 float64, y0 float64,
	x1 float64, y1 float64,
	dColor color.Color) {

	draw.IMDrawer.Color = dColor
	draw.IMDrawer.Push(
		pixel.V(x0, y0),
		pixel.V(x1, y1))
	draw.IMDrawer.Line(1.0)
}
