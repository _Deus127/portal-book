package camera

import (
	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/geom"
)

// Portal represents a "window" through which we view the next sector.
type Portal struct {
	SectorID int
	LeftX    int
	RightX   int
}

func (c *Camera) pushPortalQueue(p *Portal) int {
	c.portalQueue = append(c.portalQueue, p)
	return len(c.portalQueue) - 1
}

func (c *Camera) popPortalQueue() *Portal {
	p := c.portalQueue[0]
	c.portalQueue = c.portalQueue[1:]
	return p
}

func (c *Camera) _getNeighborInfo(x float64, y float64, z float64,
	line *geom.Line, portal *Portal, sectorList []*geom.Sector) (neighbor *geom.Sector, nsCeil float64, nsFloor float64) {

	if line.IsPortal() { // is another sector showing through this portal?

		neighborID := line.NeighborOppositeOf(x, y)
		// fmt.Println(line, neighborID)
		if neighborID >= 0 {
			neighbor = sectorList[neighborID]
		}
		nsCeil = neighbor.CeilingHeight - z
		nsFloor = neighbor.FloorHeight - z
	}
	return neighbor, nsCeil, nsFloor
}
