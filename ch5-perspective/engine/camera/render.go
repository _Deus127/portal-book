package camera

import (
	"math"

	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/drawer"
	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/geom"
	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/mathutils"
	"golang.org/x/image/colornames"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// DebugStage tells the camera which stage of the rendering to draw in debug mode.
type DebugStage int

// Constant defines for the specific DebugStages.
const (
	OVERHEAD DebugStage = iota
	TRANSLATED
	ROTATED
	PERSPECTIVE
)

func (c *Camera) _initRenderFrame() (renderWidth int, renderHeight int,
	renderedSectorCnts map[int]int) {

	renderWidth = int(c.RenderWidth)
	renderHeight = int(c.RenderHeight)
	renderedSectorCnts = map[int]int{}
	return renderWidth, renderHeight, renderedSectorCnts
}

func (c *Camera) renderFrame(win *pixelgl.Window, draw *drawer.Drawer,
	x float64, y float64, z float64,
	angle float64,
	sectorID int, sectorList []*geom.Sector, debugStage DebugStage) {

	renderWidth, renderHeight, renderedSectorCnts := c._initRenderFrame()

	// begin rendering from the prescribed position (push our sector into the queue with the full width available)
	c.pushPortalQueue(&Portal{
		SectorID: sectorID,
		LeftX:    0,
		RightX:   renderWidth - 1,
	})

	// process all portals in the order they are added
	for len(c.portalQueue) > 0 {
		currPortal := c.popPortalQueue()
		currSector := sectorList[currPortal.SectorID]

		// abort if we've already drawn this sector too many times (i.e. mirrors)
		if renderCnt, ok := renderedSectorCnts[currSector.ID]; ok && renderCnt > c.maxSectorRenderCnt {
			continue
		}
		// render the current sector as seen through the current portal
		c._renderSectorFromThrough(win, draw, x, y, z, angle,
			currPortal, currSector, sectorList, renderWidth, renderHeight, debugStage)
		renderedSectorCnts[currSector.ID]++
	}
}

func (c *Camera) _renderSectorFromThrough(win *pixelgl.Window, draw *drawer.Drawer,
	x float64, y float64, z float64,
	angle float64,
	portal *Portal, sector *geom.Sector, sectorList []*geom.Sector,
	renderWidth int, renderHeight int, debugStage DebugStage) {

	// DEBUG - CAM POS
	if debugStage != PERSPECTIVE {
		c._debugPointScaled(draw, 0, 0, colornames.Red)
	}

	anglesin := math.Sin(angle)
	anglecos := math.Cos(angle)

	for _, line := range sector.Lines {

		// DEBUG - SECTOR OVERHEAD
		if debugStage == OVERHEAD {
			c._debugLineScaled(draw, line.V0.Vec.X, line.V0.Vec.Y, line.V1.Vec.X, line.V1.Vec.Y, colornames.Olive)
		}

		// translate line around camera (flip wall vertices if it's a portal and we're viewing it from behind)
		var t0 pixel.Vec
		var t1 pixel.Vec
		// var lineSide *geom.Side
		if line.IsPortal() && (mathutils.PointSide(
			x, y,
			line.V0.Vec.X, line.V0.Vec.Y,
			line.V1.Vec.X, line.V1.Vec.Y) > 0) {

			t0 = line.V1.Vec.Sub(pixel.V(x, y))
			t1 = line.V0.Vec.Sub(pixel.V(x, y))
			// lineSide = line.Side1
		} else {
			t0 = line.V0.Vec.Sub(pixel.V(x, y))
			t1 = line.V1.Vec.Sub(pixel.V(x, y))
			// lineSide = line.Side0
		}

		// DEBUG - SECTOR TRANSLATED
		if debugStage == TRANSLATED {
			c._debugLineScaled(draw, t0.X, t0.Y, t1.X, t1.Y, colornames.Yellowgreen)
		}

		// rotate around the camera
		rt0 := pixel.V(
			(t0.X*anglecos)-(t0.Y*anglesin),
			(t0.X*anglesin)+(t0.Y*anglecos))
		rt1 := pixel.V(
			(t1.X*anglecos)-(t1.Y*anglesin),
			(t1.X*anglesin)+(t1.Y*anglecos))

		// DEBUG - SECTOR ROTATED
		if debugStage == ROTATED {
			c._debugLineScaled(draw, rt0.X, rt0.Y, rt1.X, rt1.Y, colornames.Darkred)
		}

		// no need to render at all if both sides of line are behind our viewpoint
		if rt0.Y <= 0 && rt1.Y <= 0 {
			continue
		}

		// wall partially behind camera? clip it against camera's view frustum
		if rt0.Y <= 0 || rt1.Y <= 0 || rt0.Y >= c.farZ || rt1.Y >= c.farZ {
			c._clipToViewFrustum(draw, &rt0.X, &rt0.Y, &rt1.X, &rt1.Y, debugStage)
		}

		// acquire floor and ceiling heights, relative to camera's view
		yCeil := sector.CeilingHeight - z
		yFloor := sector.FloorHeight - z

		// perspective transformation + screen space
		// screenX = W/2 + (worldX * hFOV)/worldZ
		// screenY = H/2 + (worldY * vFOV)/worldZ
		x0 := int(c.RenderWidth/2 + (rt0.X*c.hFOV)/rt0.Y)
		y0a := int(c.RenderHeight/2 + (yCeil*c.vFOV)/rt0.Y)
		y0b := int(c.RenderHeight/2 + (yFloor*c.vFOV)/rt0.Y)
		x1 := int(c.RenderWidth/2 + (rt1.X*c.hFOV)/rt1.Y)
		y1a := int(c.RenderHeight/2 + (yCeil*c.vFOV)/rt1.Y)
		y1b := int(c.RenderHeight/2 + (yFloor*c.vFOV)/rt1.Y)

		// only render if it's visible
		if x0 >= x1 || (x1 < portal.LeftX) || (x0 > portal.RightX) {
			continue
		}

		// DEBUG - SECTOR PERSPECTIVE
		if debugStage == PERSPECTIVE {
			c._debugLine(draw, float64(x0), float64(y0a), float64(x0), float64(y0b), colornames.Black) // left
			c._debugLine(draw, float64(x1), float64(y1a), float64(x1), float64(y1b), colornames.Black) // right
			c._debugLine(draw, float64(x0), float64(y0a), float64(x1), float64(y1a), colornames.Black) // top
			c._debugLine(draw, float64(x0), float64(y0b), float64(x1), float64(y1b), colornames.Black) // bottom
		}

		// do we have a neighbor through this wall?
		neighbor, _, _ := c._getNeighborInfo(x, y, z, line, portal, sectorList)

		beginX := int(math.Max(float64(x0), float64(portal.LeftX)))
		endX := int(math.Min(float64(x1), float64(portal.RightX)))

		// schedule the neighboring sector for rendering within the portal window formed by this wall
		if neighbor != nil {
			c.pushPortalQueue(&Portal{
				SectorID: neighbor.ID,
				LeftX:    beginX,
				RightX:   endX,
			})
		}
	}
}
