package game

import (
	"fmt"

	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/camera"
	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/drawer"
	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/level"
	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/mathutils"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// Game gives access to the game.
type Game struct {
	DebugCamera *camera.Camera
	debugStage  camera.DebugStage
	Level       *level.Level
}

// NewGame returns a new game instance.
func NewGame(win *pixelgl.Window) *Game {
	// fmt.Print("Loading Game...")
	g := Game{
		Level: level.LoadFromFile(level.DefaultPath + "TestMap.map"),
	}
	g.DebugCamera = camera.NewCamera(win.Bounds().W(), win.Bounds().H(), 90.0*mathutils.DegToRad)
	// fmt.Println("OK")
	return &g
}

// HandleInput handles input to the game.
func (g *Game) HandleInput(win *pixelgl.Window, dt float64) {
	if win.JustPressed(pixelgl.Key1) {
		g.debugStage = camera.OVERHEAD
	} else if win.JustPressed(pixelgl.Key2) {
		g.debugStage = camera.TRANSLATED
	} else if win.JustPressed(pixelgl.Key3) {
		g.debugStage = camera.ROTATED
	} else if win.JustPressed(pixelgl.Key4) {
		g.debugStage = camera.PERSPECTIVE
	}
}

// Update updates the game (should be called once per frame).
func (g *Game) Update(win *pixelgl.Window, draw *drawer.Drawer, dt float64) {
}

// Draw lets the game draw.
func (g *Game) Draw(win *pixelgl.Window, draw *drawer.Drawer) {
	// win.Clear(colornames.Black)
	player := g.Level.Entities[0]
	g.DebugCamera.Draw(win, draw,
		player.Position.X(), player.Position.Y(), 1, // Z
		player.Angle, player.SectorID, g.Level.Sectors, g.debugStage)
	fmt.Fprintf(draw.TxtDrawer, "\n\n1 - OVERHEAD\n2 - TRANSLATED\n3 - ROTATED\n4 - PERSPECTIVE")
	draw.TxtDrawer.Draw(win, pixel.IM.Scaled(draw.TxtDrawer.Orig, draw.TxtDrawerScale))
	draw.TxtDrawer.Clear()
}

// OnShutdown lets the game know the engine is shutting down.
func (g *Game) OnShutdown() {
}
