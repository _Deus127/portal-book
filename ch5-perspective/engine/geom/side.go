package geom

// Side holds a side definition for a line.
type Side struct {
	TouchesSectorID int
}

// NewSide returns a new side instance.
func NewSide(
	touchesSectorID int) *Side {

	return &Side{
		TouchesSectorID: touchesSectorID,
	}
}
