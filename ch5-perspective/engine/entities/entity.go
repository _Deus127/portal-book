package entities

import (
	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/geom"
	"bitbucket.org/_Deus127/portal-book/ch5-perspective/engine/mathutils"

	"github.com/go-gl/mathgl/mgl64"
)

// Entity is a basic game engine entity.
type Entity struct {
	Position *mgl64.Vec3
	Velocity *mgl64.Vec3
	Angle    float64
	SectorID int
}

// NewEntity returns a new entity instance.
func NewEntity(eType string, pos mgl64.Vec3, vel mgl64.Vec3, angle float64, sectorID int) *Entity {
	e := Entity{
		Position: &pos,
		Velocity: &vel,
		Angle:    angle,
		SectorID: sectorID,
	}
	return &e
}

// Move moves the entity by (dx,dy) in the map
func (e *Entity) Move(dx float64, dy float64, sectors []*geom.Sector) {
	// check if we cross sector edge with a neighbor on the other side
	// NOTE: line vertices of each sector are defined in
	// clockwise order so PointSide will always return -1 for a point
	// outside the sector and 0 or 1 for a point that is inside
	eSector := sectors[e.SectorID]

	for _, line := range eSector.Lines {
		if line.IsPortal() &&
			mathutils.IntersectBox(
				e.Position.X(), e.Position.Y(),
				e.Position.X()+dx, e.Position.Y()+dy,
				line.V0.Vec.X, line.V0.Vec.Y,
				line.V1.Vec.X, line.V1.Vec.Y) {

			if mathutils.PointSide(
				e.Position.X()+dx, e.Position.Y()+dy,
				line.V0.Vec.X, line.V0.Vec.Y,
				line.V1.Vec.X, line.V1.Vec.Y) > 0 {

				e.SectorID = line.Side1.TouchesSectorID
			} else {
				e.SectorID = line.Side0.TouchesSectorID
			}
		}
	}
	*e.Position = e.Position.Add(mgl64.Vec3{dx, dy, 0.0})
}
