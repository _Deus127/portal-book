package framerate

import (
	"fmt"

	"bitbucket.org/_Deus127/portal-book/ch2-framework/engine/drawer"

	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
)

// Monitor monitors framerate and delta time.
type Monitor struct {
	FPS                float64
	fpsFrameCount      int
	fpsFrameDT         float64
	fpsUpdateThreshold float64
}

// NewMonitor returns a new framerate monitor instance.
func NewMonitor() *Monitor {
	return &Monitor{
		fpsFrameCount:      0,
		fpsFrameDT:         0.0,
		fpsUpdateThreshold: 1.0 / 4.0, // 1 OVER UPDATES PER SEC
	}
}

// Update updates the monitor (should be called once per frame).
func (fm *Monitor) Update(dt float64) {
	fm.fpsFrameCount++
	fm.fpsFrameDT += dt

	if fm.fpsFrameDT >= fm.fpsUpdateThreshold {
		fm.FPS = float64(fm.fpsFrameCount) / fm.fpsFrameDT
		fm.fpsFrameCount = 0
		fm.fpsFrameDT = 0.0
	}
}

// Draw draws the monitor.
func (fm *Monitor) Draw(win *pixelgl.Window, draw *drawer.Drawer) {
	fmt.Fprintf(draw.TxtDrawer, "FPS: %.2f", fm.FPS)
	draw.TxtDrawer.Draw(win, pixel.IM.Scaled(draw.TxtDrawer.Orig, draw.TxtDrawerScale))
	draw.TxtDrawer.Clear()
}
