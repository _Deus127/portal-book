package mathutils

import (
	"math"

	"github.com/faiface/pixel"
)

// Conversion constants.
const (
	RadToDeg  = 180 / math.Pi
	DegToRad  = math.Pi / 180
	RadToGrad = 200 / math.Pi
	GradToDeg = math.Pi / 200
)

// Round rounds a float to the nearest unit specified (i.e. 0.1, 0.05, etc).
func Round(x, unit float64) float64 {
	return math.Round(x/unit) * unit
}

// RangesOverlap determines whether two number ranges overlap.
func RangesOverlap(a0 float64, a1 float64, b0 float64, b1 float64) bool {
	return (math.Min(a0, a1) <= math.Max(b0, b1)) && (math.Min(b0, b1) <= math.Max(a0, a1))
}

// IntersectBox determine whether two 2D-boxes intersect.
func IntersectBox(ax0 float64, ay0 float64, ax1 float64, ay1 float64, bx0 float64, by0 float64, bx1 float64, by1 float64) bool {
	return RangesOverlap(ax0, ax1, bx0, bx1) && RangesOverlap(ay0, ay1, by0, by1)
}

// VCross returns the vector cross product.
func VCross(x0 float64, y0 float64, x1 float64, y1 float64) float64 {
	return (x0)*(y1) - (x1)*(y0)
}

// PointSide determines which side of a line the point is on.
// Returns: <0, =0 or >0
func PointSide(px float64, py float64, x0 float64, y0 float64, x1 float64, y1 float64) float64 {
	return VCross((x1)-(x0), (y1)-(y0), (px)-(x0), (py)-(y0))
}

// IntersectPoint calculate the point of intersection between two lines.
func IntersectPoint(ax0 float64, ay0 float64, ax1 float64, ay1 float64,
	bx0 float64, by0 float64, bx1 float64, by1 float64) pixel.Vec {

	return pixel.V(
		VCross(VCross(ax0, ay0, ax1, ay1), (ax0)-(ax1), VCross(bx0, by0, bx1, by1), (bx0)-(bx1))/
			VCross((ax0)-(ax1), (ay0)-(ay1), (bx0)-(bx1), (by0)-(by1)),

		VCross(VCross(ax0, ay0, ax1, ay1), (ay0)-(ay1), VCross(bx0, by0, bx1, by1), (by0)-(by1))/
			VCross((ax0)-(ax1), (ay0)-(ay1), (bx0)-(bx1), (by0)-(by1)))
}
