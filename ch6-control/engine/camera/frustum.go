package camera

import (
	"bitbucket.org/_Deus127/portal-book/ch6-control/engine/drawer"
	"bitbucket.org/_Deus127/portal-book/ch6-control/engine/mathutils"
	"golang.org/x/image/colornames"
)

func (c *Camera) _clipToViewFrustum(draw *drawer.Drawer, tx0 *float64, tz0 *float64, tx1 *float64, tz1 *float64, debugStage DebugStage) {
	// find intersection between wall and the approximate edges of camera's view
	i0 := mathutils.IntersectPoint(*tx0, *tz0, *tx1, *tz1, -c.nearSide, c.nearZ, c.nearSide, c.nearZ)
	i1 := mathutils.IntersectPoint(*tx0, *tz0, *tx1, *tz1, -c.farSide, c.farZ, c.farSide, c.farZ)

	if debugStage == ROTATED {
		c._debugLineScaled(draw, -c.nearSide, c.nearZ, c.nearSide, c.nearZ, colornames.Blueviolet) // near
		c._debugLineScaled(draw, -c.farSide, c.farZ, c.farSide, c.farZ, colornames.Blueviolet)     // far
	}

	// fmt.Printf("P: (%#v, %#v) <-> (%#v, %#v)\n", *tx0, *tz0, *tx1, *tz1)
	// fmt.Printf("INT: %#v\n%#v\n", i0, i1)
	if *tz0 < c.nearZ {
		if i0.Y > 0 {
			*tx0 = i0.X
			*tz0 = i0.Y
			if debugStage == ROTATED {
				c._debugPointScaled(draw, *tx0, *tz0, colornames.Red)
			}
		}
	} else if *tz0 > c.farZ {
		if i1.Y > 0 {
			*tx0 = i1.X
			*tz0 = i1.Y
		} else {
			*tz0 = i0.Y
		}
		if debugStage == ROTATED {
			c._debugPointScaled(draw, *tx0, *tz0, colornames.Yellow)
		}
	}

	if *tz1 < c.nearZ {
		if i0.Y > 0 {
			*tx1 = i0.X
			*tz1 = i0.Y
			if debugStage == ROTATED {
				c._debugPointScaled(draw, *tx1, *tz1, colornames.Green)
			}
		}
	} else if *tz1 > c.farZ {
		if i1.Y > 0 {
			*tx1 = i1.X
			*tz1 = i1.Y
		} else {
			*tz1 = i0.Y
		}
		if debugStage == ROTATED {
			c._debugPointScaled(draw, *tx1, *tz1, colornames.Blue)
		}
	}
}
